## Run app
- open index.html in browser
## Questions
- How long did it take you to complete this assignment?


- What about this assignment did you find most challenging?


- What about this assignment did you find unclear?


- What challenges did you face that you did not expect?


- Do you feel like this assignment has an appropriate level of difficulty?

- Briefly explain your decisions to use tools, frameworks and libraries like React, Vue, etc.


## Answers

- The main tasks take 2 hours. Also I've implemented persisting state on refresh that takes 15 minutes.
- I think the most challenging feature for me is "instead of drawing static images, drawing custom object (like our alerts)".
- Hah, I tried to set border for image as simple CSS border :). Then I understood that it shouldn't work this way.
- If we are talking about main tasks, I would say no, but additional points are harder.
- I did not use any additional tools because I wanted to show you my skills in pure JS and canvas. But we can move this solution to PIXI.js lib,
which has a lot of useful instruments for future client features and can be easily adapt for phones.
