const imgUrls = ['./assets/dog.png', './assets/cat.png'];

const state = {
  images: [],
  canvas: null,
  canvasContext: null,
  startMouseX: 0,
  startMouseY: 0,
};

function getSavedData() {
  return JSON.parse(sessionStorage.getItem('prevImagesData')) || [];
}

async function loadImages(imageUrls) {
  const images = [];
  const prevImagesData = getSavedData();
  for (let i = 0; i < imgUrls.length; i++) {
    try {
      const element = await loadImage(imageUrls[i]);
      const data = {
        x: 0,
        y: 0,
        width: element.width,
        height: element.height,
        isDragging: false,
      };

      if (prevImagesData.length) {
        data.x = prevImagesData[i].data.x;
        data.y = prevImagesData[i].data.y;
      }

      images.push({
        element,
        data,
      });
    } catch (err) {
      console.error(err.message);
    }
  }
  return images;
}

function loadImage(src) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.addEventListener('load', () => {
      resolve(image);
    });
    image.addEventListener('error', reject);
    image.src = src;
  });
}

function render() {
  state.canvasContext.clearRect(0, 0, state.canvas.width, state.canvas.height);
  drawImages();
}


function drawImages() {
  state.images.forEach(image => {
    const imageRect = new Path2D();
    imageRect.rect(image.data.x, image.data.y, image.data.width, image.data.height);

    // set border for draggable image
    if (image.data.isDragging) {
      state.canvasContext.strokeStyle = 'green';
      state.canvasContext.lineWidth = 2;
      state.canvasContext.stroke(imageRect)
    }

    image.path = imageRect;
    state.canvasContext.drawImage(
      image.element,
      image.data.x,
      image.data.y,
      image.data.width,
      image.data.height
    );
  });
}

function adjustCanvasSize() {
  const { innerWidth } = window;
  state.canvas.width = innerWidth;
  state.canvas.height = Math.floor((innerWidth / 16) * 9);
}

function saveCurrentCanvasState() {
  const data = JSON.stringify(state.images);
  sessionStorage.setItem('prevImagesData', data);
}

function mouseUpEvent(e) {
  e.preventDefault();
  e.stopPropagation();

  // clear all the dragging flags
  state.images.forEach((image) => {
    image.data.isDragging = false;
  });

  saveCurrentCanvasState();
}

function isMouseInImage(mouseX, mouseY, {data: {x, y, width, height}}) {
  return (
      mouseX > x &&
      mouseX < x + width &&
      mouseY > y &&
      mouseY < y + height
  )
}

function getCurrentMousePosition(e) {
  if(!e) {
    return {
      mouseX: 0,
      mouseY: 0,
    };
  }

  const {canvas} = state;
  const canvasRect = canvas.getBoundingClientRect();
  const offsetX = canvasRect.left;
  const offsetY = canvasRect.top;
  const mouseX = e.clientX - offsetX;
  const mouseY = e.clientY - offsetY;

  return {mouseX, mouseY};
}

function mouseDownEvent(e) {
  e.preventDefault();
  e.stopPropagation();

  const {images} = state;
  // get the current mouse position
  const {mouseX, mouseY} = getCurrentMousePosition(e);

  // test each rect to see if mouse is inside
  images.forEach((image) => {
    if(isMouseInImage(mouseX, mouseY, image)) {
      image.data.isDragging = true;
    }
  });

  // save the current mouse position
  state.startMouseX = mouseX;
  state.startMouseY = mouseY;

}

function isImageOutsideCanvas(image) {
  const {data: {x, y, height: imageH, width: imageW}} = image;
  const {canvas: {height, width}} = state;
  return {
    outsideBottom: y + imageH > height,
    outsideRight: x + imageW > width,
    outsideLeft: x <= 0,
    outsideTop: y <= 0,
  };

}

function mouseMoveEvent(e) {
  e.preventDefault();
  e.stopPropagation();
  const {images, startMouseX, startMouseY} = state;

  // get the current mouse position
  const {mouseX, mouseY} = getCurrentMousePosition(e);
  // calculate the distance the mouse has moved
  // since the last mousemove
  const dx = mouseX - startMouseX;
  const dy = mouseY - startMouseY;

  // move each image that isDragging
  // by the distance the mouse has moved
  // since the last mousemove
  images.forEach((image) => {
    if(image.data.isDragging) {
      image.data.x += dx;
      image.data.y += dy;
      const {
        outsideBottom, outsideRight,
        outsideLeft, outsideTop
      } = isImageOutsideCanvas(image);

      if(outsideBottom) {
        image.data.y = state.canvas.height - image.data.height;
      }
      if (outsideRight) {
        image.data.x = state.canvas.width - image.data.width;
      }
      if(outsideLeft) {
        image.data.x = 0;
      }
      if (outsideTop) {
        image.data.y = 0;
      }

    }
  });

  // redraw the scene with the new rect positions
  render();

  // reset the starting mouse position for the next mousemove
  state.startMouseX = mouseX;
  state.startMouseY = mouseY;
}

async function setInitialState() {
  state.images = await loadImages(imgUrls);
  state.canvas = document.getElementById('canvas');
  state.canvasContext = state.canvas.getContext('2d');

  // canvas events
  state.canvas.onmousedown = mouseDownEvent;
  state.canvas.onmouseup = mouseUpEvent;
  state.canvas.onmousemove = mouseMoveEvent;
}

window.addEventListener('load', async () => {
  await setInitialState();
  adjustCanvasSize();
  render();
});

window.addEventListener('resize', () => {
  adjustCanvasSize();
  render();
});

